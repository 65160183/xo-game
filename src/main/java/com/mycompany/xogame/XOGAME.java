/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.xogame;
import java.util.Scanner;

public class XOGAME {
    static int end = 0;
    public static void main(String[] args) {
        String[][] array = new String[3][3];
        Scanner kb = new Scanner(System.in);
        boolean reset = false;
        while (!reset) {
            boolean winner = false;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    array[i][j] = "_ ";
                }
            }
            while (!winner) {
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        System.out.print(array[i][j] + " ");
                    }
                    System.out.println();
                }
                System.out.println("Turn X");
                System.out.println();
                int rowX = kb.nextInt();
                int colX = kb.nextInt();
                System.out.println();
                end+=1;
                array[rowX][colX] = "X ";
                if ((array[0][0].equals("X ") && array[0][1].equals("X ") && array[0][2].equals("X ")) ||
                    (array[1][0].equals("X ") && array[1][1].equals("X ") && array[1][2].equals("X ")) ||
                    (array[2][0].equals("X ") && array[2][1].equals("X ") && array[2][2].equals("X ")) ||
                    (array[0][0].equals("X ") && array[1][0].equals("X ") && array[2][0].equals("X ")) ||
                    (array[0][1].equals("X ") && array[1][1].equals("X ") && array[2][1].equals("X ")) ||
                    (array[0][2].equals("X ") && array[1][2].equals("X ") && array[2][2].equals("X ")) ||
                    (array[0][0].equals("X ") && array[1][1].equals("X ") && array[2][2].equals("X ")) ||
                    (array[0][2].equals("X ") && array[1][1].equals("X ") && array[2][0].equals("X "))) {
                    winner = true;
                    System.out.println();
                    for (int i = 0; i < 3; i++) {
                        for (int j = 0; j < 3; j++) {
                            System.out.print(array[i][j] + " ");
                    }
                    System.out.println();
                }
                    System.out.println();
                    System.out.println("Winner is X");
                    break;
                }
                if(end == 9){
                    System.out.println("DRAW");
                    for (int i = 0; i < 3; i++) {
                        for (int j = 0; j < 3; j++) {
                            System.out.print(array[i][j] + " ");
                    }
                        System.out.println();
                }
                    System.out.println();
                    break; 
                }
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        System.out.print(array[i][j] + " ");
                    }
                    System.out.println();
                }
                if (winner) {
                    break;                
                }
                System.out.println("Turn O");
                System.out.println();
                int rowO = kb.nextInt();
                int colO = kb.nextInt();
                System.out.println();
                end +=1; 
                array[rowO][colO] = "O ";
                if ((array[0][0].equals("O ") && array[0][1].equals("O ") && array[0][2].equals("O ")) ||
                    (array[1][0].equals("O ") && array[1][1].equals("O ") && array[1][2].equals("O ")) ||
                    (array[2][0].equals("O ") && array[2][1].equals("O ") && array[2][2].equals("O ")) ||
                    (array[0][0].equals("O ") && array[1][0].equals("O ") && array[2][0].equals("O ")) ||
                    (array[0][1].equals("O ") && array[1][1].equals("O ") && array[2][1].equals("O ")) ||
                    (array[0][2].equals("O ") && array[1][2].equals("O ") && array[2][2].equals("O ")) ||
                    (array[0][0].equals("O ") && array[1][1].equals("O ") && array[2][2].equals("O ")) ||
                    (array[0][2].equals("O ") && array[1][1].equals("O ") && array[2][0].equals("O "))) {
                    winner = true;
                    System.out.println();
                    for (int i = 0; i < 3; i++) {
                        for (int j = 0; j < 3; j++) {
                            System.out.print(array[i][j] + " ");
                    }
                    System.out.println();
                }
                    System.out.println();
                    System.out.println("Winner is O");
                    break;     
                }
                
            }
            System.out.println("Reset ?(YES/NO)");
            String re = kb.next();
            if (re.equals("YES")){
                reset = false;   
                end = 0;
            }else{
                reset = true;
            }
        }
    }
}
